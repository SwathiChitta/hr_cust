from openerp.osv import fields, osv
from openerp.tools.translate import _
import time



class expiry_report(osv.osv):


	_name = "expiry.report"

	_columns = {
	"from_date": fields.date("From Date"),
	"to_date": fields.date("To Date"),
	'doc': fields.selection([
			('eid_no', 'EID '),
			('visa_no', 'Visa '),
			('medical_no','Medical Insurance')
		],'Document'),
	}

	def action_expiry_details(self,cr,uid,ids,context=None):
		input_data = self.read(cr, uid, ids)[0]

		if input_data['doc'] == 'eid_no':
			res_ids = self.pool.get('hr.employee').search(cr, uid, [('eid_expiry','>=',input_data['from_date']),('eid_expiry','<=',input_data['to_date'])])
	
		elif input_data['doc'] == 'visa_no':
			res_ids = self.pool.get('hr.employee').search(cr, uid, [('visa_expiry','>=',input_data['from_date']),('visa_expiry','<=',input_data['to_date'])])
	
		elif input_data['doc'] == 'medical_no':
			res_ids = self.pool.get('hr.employee').search(cr, uid, [('medical_expiry','>=',input_data['from_date']),('medical_expiry','<=',input_data['to_date'])])
		else:
		 	res_ids = self.pool.get('hr.employee').search(cr, uid,[('eid_expiry','>=',input_data['from_date']),('eid_expiry','<=',input_data['to_date'])])
		 	res_ids = res_ids + self.pool.get('hr.employee').search(cr, uid,[('visa_expiry','>=',input_data['from_date']),('visa_expiry','<=',input_data['to_date'])])
		 	res_ids = res_ids + self.pool.get('hr.employee').search(cr, uid,[('medical_expiry','>=',input_data['from_date']),('medical_expiry','<=',input_data['to_date'])])

		models_data = self.pool.get('ir.model.data')       

		expiry_report_tree = models_data._get_id(cr, uid, 'hrms', 'employee_form_view_inherit')

		return {
			'name': 'Expiry Report',
			'view_type': 'form',
			"view_mode": 'tree,form',
			'res_model': 'hr.employee',
			'type': 'ir.actions.act_window',
			'search_view_id': expiry_report_tree,
			# 'res_id': res_ids,
			'domain':"[('id', 'in',%s)]" %(res_ids),
		}



	


	# def action_expiry_details(self,cr,uid,ids,context=None):

	# 	input_data = self.read(cr, uid, ids)[0]

	# 	if input_data['from_date'] > input_data['to_date']:
	# 		raise osv.except_osv(_('Invalid Action!'), _('From Date must be less than To Date !'))

	# 	if input_data['eid_no'] == True:
	# 		res_ids = self.pool.get('hr.employee').search(cr, uid, [('eid_expiry','>=',input_data['from_date']),('eid_expiry','<=',input_data['to_date'])])
	# 	else:
	# 		res_ids = self.pool.get('hr.employee').search(cr, uid, [('eid_expiry','>=',input_data['from_date']),('eid_expiry','<=',input_data['to_date'])])
		
	# 	# if input_data['doc'] == 'visa_no':
	# 	# 	res_ids = self.pool.get('hr.employee').search(cr, uid, [('eid_expiry','>=',input_data['from_date']),('eid_expiry','<=',input_data['to_date']),('eid_no','=',input_data['eid_no'][0])])
	# 	# else:
	# 	# 	res_ids = self.pool.get('hr.employee').search(cr, uid, [('eid_expiry','>=',input_data['from_date']),('eid_expiry','<=',input_data['to_date'])])
		
	# 	# if input_data['doc'] == 'eid_no':
	# 	# 	res_ids = self.pool.get('hr.employee').search(cr, uid, [('eid_expiry','>=',input_data['from_date']),('eid_expiry','<=',input_data['to_date']),('eid_no','=',input_data['eid_no'][0])])
	# 	# else:
	# 	# 	res_ids = self.pool.get('hr.employee').search(cr, uid, [('eid_expiry','>=',input_data['from_date']),('eid_expiry','<=',input_data['to_date'])])
		
	# 	models_data = self.pool.get('ir.model.data')       

	# 	expiry_report_tree = models_data._get_id(cr, uid, 'hr_cust', 'employee_form_view_inherit')

	# 	return {
	# 		'name': 'Expiry Info',
	# 		'view_type': 'form',
	# 		"view_mode": 'tree,form',
	# 		'res_model': 'hr.employee',
	# 		'type': 'ir.actions.act_window',
	# 		'search_view_id': expiry_report_tree,
	# 		# 'res_id': res_ids,
	# 		'domain':"[('id', 'in',%s)]" %(res_ids),
	# 	}

	# def action_expiry_details(self,cr,uid,ids,context=None):

	# 	data = self.read(cr, uid, ids)[0]

	# 	if data == 'eid_no' :
	# 		expiry_ids = self.pool.get("hr.employee").search(cr,uid,[("eid_expiry",">=",data["from_date"]),("eid_expiry","<=",data["to_date"])])       
		   

	# 	if data == 'visa_no':
	# 		expiry_ids = self.pool.get("hr.employee").search(cr,uid,[("visa_expiry",">=",data["from_date"]),("visa_expiry","<=",data["to_date"])])       
	# 		print expiry_ids

	# 	if data == 'medical_no':
	# 		expiry_ids = self.pool.get("hr.employee").search(cr,uid,[("medical_expiry",">=",data["from_date"]),("medical_expiry","<=",data["to_date"])])       
	# 		print expiry_ids

	# 	models_data = self.pool.get("ir.model.data")
	# 	expiry_report_tree = models_data._get_id(cr, uid, "hr_cust", "employee_form_view_inherit")
	# 	return{"name":"expiry Info","view_type":"form","view_mode":"tree,form","res_model":"hr.employee","type": "ir.actions.act_window",
	# 	"search_view_id": expiry_report_tree,'domain':"[('id', 'in',%s)]" %(expiry_ids),



	# 	}

		# return {
	 #    "name": "expiry Info",
		# "view_type": "form",
		# "view_mode": "tree,form",
		# "res_model": "expiry.details",
		# "type": "ir.actions.act_window",
		# "search_view_id": expiry_report_tree,
		# "domain":"[("id", "in",%s)]" %(expiry_ids),
		# }
		
	 
 
expiry_report()  
