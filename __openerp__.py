{
    'name': 'Employee Directory customized',
    'version': '1.1',
    'author': 'ehAPI',
    'category': 'Human Resources',
    'sequence': 21,
    'website': 'https://www.ehapi.com',
    'summary': 'Jobs, Departments, Employees Details with visa, EID and medical details',
    'description': """
Human Resources Management
==========================

This application enables you to manage important aspects of your company's staff and other details such as their skills, contacts, working time...


You can manage:
---------------
* Employees and hierarchies : You can define your employee with User and display hierarchies
* HR Departments
* HR Jobs
    """,
    'website': 'https://www.odoo.com/page/employees',
    'depends': ['base_setup','mail', 'resource', 'board',
                'hr','hr_recruitment', 'hr_applicant_document','hr_holidays',
                'hr_evaluation','account_asset',
                'hr_payroll','account','disable_openerp_online',
                'document','email_template',
                'fetchmail','hr_contract','report'],
    'data': [
        'security/hr_cust_security_view.xml',
        'security/ir.model.access.csv',
        'wizard/expiry_report_wizard_view.xml',
        'wizard/expiry_report_view.xml',
        'hr_cust_view.xml',
        'hr_applicant_cust_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
