from openerp.osv import fields, osv

class hr_employee(osv.osv):
    _inherit = "hr.employee"
    _columns = {
    'eid_expiry' : fields.date('EID expiry date'),
    'visa_expiry' : fields.date('Visa expiry date'),
    'medical_expiry' : fields.date('Medical Insurance expiry date'),
    'eid_no' : fields.char('EID No.'),
    'visa_no' : fields.char('Visa No.'),
    'medical_no' : fields.char('Medical Insurance No.'),
    'asset_hand' : fields.one2many('asset.handover', 'rel_to_hr', 'Account Hand Over'),      
    }

class asset_handover(osv.osv):
    _name = "asset.handover"
    _columns = {
    'asset_name' : fields.many2one('account.asset.asset','Asset Name', domain="[('employee','=',False)]"),
    'asset_code' : fields.char('Asset Code'),
    'quantity' : fields.integer('Quantity'),
    'remarks' : fields.text('Remarks'),
    'rel_to_hr':fields.many2one('hr.employee','Rel'),
	}

class account_asset_employee(osv.osv):
    _inherit = "account.asset.asset"
    _columns = {
    'employee' : fields.many2one('hr.employee', 'Employee', help="Asset handed over to this Employee")
    }



class custom_hr_contract(osv.osv):
    _inherit = "hr.contract"
    _columns = {
    'prohibition_no' : fields.integer('hr.employee', 'Probation period'),
    'prohibition_type' : fields.selection([('days','Days'),
                                            ('week','Weeks'),
                                            ('month','Months')], 'Probation period'),
    }